#include "boardtile.h"

BoardTile::BoardTile(QObject *parent, BoardTileType type, float additionalGold, int additionalExperience)
    : QObject{parent}
{
    m_type = type;
    m_additionalGold = additionalGold;
    m_additionalExperience = additionalExperience;
}

BoardTile* BoardTile::getNewTile(BoardTileType type){
    switch (type) {
        case BoardTileType::empty_tile:

            break;
        case BoardTileType::house:
            return new BoardTile(nullptr, BoardTileType::house, 10, 10);
            break;
        case BoardTileType::blacksmith:
            return new BoardTile(nullptr, BoardTileType::blacksmith, 20, 10);
            break;
        case BoardTileType::forester:
            return new BoardTile(nullptr, BoardTileType::forester, 10, 10);
            break;
        case BoardTileType::church:
            return new BoardTile(nullptr, BoardTileType::church, 30, 10);
            break;
        case BoardTileType::townhall:
            return new BoardTile(nullptr, BoardTileType::townhall, 50, 10);
            break;
        default:
            return new BoardTile(nullptr, BoardTileType::empty_tile, 0, 0);
    }
}
BoardTileType BoardTile::getType(){
    return m_type;
}
float BoardTile::getAdditionalGold(){
    return m_additionalGold;
}
int BoardTile::getAdditionalExperience(){
    return m_additionalExperience;
}


QString BoardTile::getTileType(){
    if (m_type == BoardTileType::blacksmith)
        return "blacksmith";

    else if(m_type == BoardTileType::church)
        return "church";

    else if(m_type == BoardTileType::forester)
        return "forester";

    else if(m_type == BoardTileType::house)
        return "house";

    else if(m_type == BoardTileType::townhall)
        return "townhall";

    else
        return "empty_tile";

}
