#include "playingboard.h"

PlayingBoard::PlayingBoard(QObject *parent)
    : QObject{parent}
{
    //vyplneni boardu praznymi tiles
    m_board.fill(getNewTile(BoardTileType::empty_tile));    
}

PlayingBoard::~PlayingBoard(){

}

void PlayingBoard::setTileValue(int index, BoardTile* newValue){
    m_board.at(index) = newValue;
}

BoardTile* PlayingBoard::getTileValue(int index){
    return m_board.at(index);
}

BoardTile* PlayingBoard::getNewTile(BoardTileType type){
    switch (type) {
        case BoardTileType::empty_tile:
            return new BoardTile(nullptr, BoardTileType::empty_tile, 0, 0);
            break;
        case BoardTileType::house:
            return new BoardTile(nullptr, BoardTileType::house, 5, 10);
            break;
        case BoardTileType::blacksmith:
            return new BoardTile(nullptr, BoardTileType::blacksmith, 5, 10);
            break;
        case BoardTileType::forester:
            return new BoardTile(nullptr, BoardTileType::forester, 5, 10);
            break;
        case BoardTileType::church:
            return new BoardTile(nullptr, BoardTileType::church, 5, 10);
            break;
        case BoardTileType::townhall:
            return new BoardTile(nullptr, BoardTileType::townhall, 5, 10);
            break;
        default:
            return new BoardTile(nullptr, BoardTileType::empty_tile, 0, 0);
    }
}
