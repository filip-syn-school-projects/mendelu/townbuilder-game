#include "filemanager.h"
#include "QDir"

FileManager::FileManager()
{

}
void FileManager::saveToFile(PlayingBoard* board, float goldPieces, int experience, int population){

    QFile file("C:/Users/mrlat/townbuilder-game/townbuilder/test_file.xml");
    if (file.open(QIODevice::WriteOnly)){
        QXmlStreamWriter xmlWriter;
        xmlWriter.setDevice(&file);

        xmlWriter.writeStartDocument();
        xmlWriter.writeStartElement("Vysledky");

        xmlWriter.writeStartElement("goldpieces");
        xmlWriter.writeCharacters(QString::number(goldPieces));
        xmlWriter.writeEndElement();

        xmlWriter.writeStartElement("experience");
        xmlWriter.writeCharacters(QString::number(experience));
        xmlWriter.writeEndElement();

        xmlWriter.writeStartElement("population");
        xmlWriter.writeCharacters(QString::number(population));
        xmlWriter.writeEndElement();

        for (int i=0; i<10; i++){
            xmlWriter.writeStartElement("row");
            xmlWriter.writeAttribute("id", QString::number(i));
            for (int y=0; y<10; y++){
                xmlWriter.writeStartElement("item");
                xmlWriter.writeAttribute("id", QString::number(i*10+y));
                xmlWriter.writeCharacters(board->getTileValue(i*10+y)->getTileType());
                xmlWriter.writeEndElement();
            }
            xmlWriter.writeEndElement();
        }

        xmlWriter.writeEndElement();
        xmlWriter.writeEndDocument();
    }

}

gameData FileManager::loadFromFile(QString file_name){
    gameData data;

    //otevreni souboru
    QFile file(file_name);
    if (file.open(QIODevice::ReadOnly)){
        int position = 0;
        //napojeni XML parseru
        QXmlStreamReader xmlReader;
        xmlReader.setDevice(&file);
        xmlReader.readNext();
        //dokud neni konec, tak bude probihat hlavni smycka
        while (!xmlReader.isEndDocument()){
            //kontrola prichoziho elementu
            if (xmlReader.isStartElement()){
                QString name = xmlReader.name().toString();
                if(name=="goldpieces"){
                    data.goldPieces=xmlReader.readElementText().toFloat();
                }
                if(name=="experience"){
                    data.experiencePoints=xmlReader.readElementText().toInt();
                }
                if(name=="population"){
                    data.population=xmlReader.readElementText().toInt();
                }
                if(name=="item"){
                    data.board.at(position)=BoardTileTypeConversion::BoardTileTypeToEnum(xmlReader.readElementText().toStdString());
                    position++;
               }
            }
            //nacteni dalsiho elementu
            xmlReader.readNext();
        }
    } else {
    qCritical() << "Soubor se nepodarilo otevrit\n";
    }

    return data;
}

