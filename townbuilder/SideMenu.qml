import QtQuick 2.0
import QtQml.Models 2.15


Rectangle {
    id: sideMenu
    property alias itemSpacing: sideMenuColumn.spacing

    width: parent.width / 10
    height: parent.height - topBar.height

    visible: true
    color: "darkgray"
    anchors.left: parent.left
    anchors.bottom: parent.bottom

    Column {
        id: sideMenuColumn
        spacing: 10
        height: parent.height
        width: parent.width
        y:itemSpacing

        ListModel {
            id: sideButtons
            ListElement {
                pic: "images/cursor.png"
                type: "empty_hand"
            }

            ListElement {
                pic: "images/house_new.png"
                type: "build_house"
            }

            ListElement {
                pic: "images/blacksmith"
                type: "build_blacksmith"
            }

            ListElement {
                pic: "images/church"
                type: "build_church"
            }

            ListElement {
                pic: "images/townhall"
                type: "build_townhall"
            }

            ListElement {
                pic: "images/destroy"
                type: "destroy_building"
            }

            ListElement {
                pic: "images/save"
                type: "save_file"
            }

            ListElement {
                pic: "images/load"
                type: "load_file"
            }

            ListElement {
                pic: "images/quit"
                type: "quit_game"
            }
        }

        Repeater {
            model:sideButtons
            SideMenuButton {
                icon: model.pic
                buttonType: model.type
            }
        }
    }

}

