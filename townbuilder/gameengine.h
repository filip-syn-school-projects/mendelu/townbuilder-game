#ifndef GAMEENGINE_H
#define GAMEENGINE_H

#include <QObject>
#include <iostream>
#include "playingboard.h"
#include "SidebarTool.h"
#include "filemanager.h"
#include "sidebartoolconversion.h"
#include "QCoreApplication"



class GameEngine : public QObject
{
    Q_OBJECT

public:

    explicit GameEngine(QObject *parent = nullptr);
    Q_INVOKABLE void loadFromFile(QString file_name);
    Q_INVOKABLE void saveToFile();
    PlayingBoard* getPlayingBoard();
    Q_INVOKABLE int getPopulation();
    Q_INVOKABLE void decreasePopulation(int position);
    Q_INVOKABLE float getGoldPieces();
    Q_INVOKABLE int getExperiencePoints();
    Q_INVOKABLE void addGoldPieces();
    Q_INVOKABLE void deductGoldPieces();
    Q_INVOKABLE bool enoughCredits();
    void addExperiencePoints(int ammount);
    Q_INVOKABLE void addPopulation(int position);
    Q_INVOKABLE SidebarTool getCurrentTool();
    Q_INVOKABLE QString getCurrentToolQString();
    Q_INVOKABLE void setCurrentTool(SidebarTool newTool);
    Q_INVOKABLE void setCurrentToolQString(QString tools);
    Q_INVOKABLE void quitGame();
    Q_INVOKABLE void printPos(int pos);
    Q_INVOKABLE void setTileState(int pos, QString state);
    Q_INVOKABLE QString getTileState(int pos);    

signals:

private:
    PlayingBoard* m_board;
    float m_goldPieces;
    int m_experiencePoints;
    int m_population;
    float m_rate;
    SidebarTool m_currentTool;

};

#endif // GAMEENGINE_H
