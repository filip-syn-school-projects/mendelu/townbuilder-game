import QtQuick 2.0

Rectangle {
    id: playingBoard
    property int boardCols : 10
    property int boardRows : 10
    property alias boardSpacing: boardGrid.spacing

    width: parent.width - sideMenu.width
    height: parent.height - topBar.height


    color: "transparent"
    anchors.right: parent.right
    anchors.bottom: parent.bottom

    Image {
        // Textura hracího pole
        id: playingBoardBackground
        fillMode: Image.Tile
        height: parent.height
        width: parent.width
        source: "images/grass-texture.png"
    }

    Grid {
        id: boardGrid

        x: boardSpacing
        y: boardSpacing

        height: parent.height - boardSpacing
        width: parent.width - boardSpacing

        rows: boardRows
        spacing: 25
        anchors.centerIn: parent
        Timer{

        }
        Repeater {
            model: boardCols * boardRows
            BoardTile {
                position: index
                state: game.getTileState(index)
            }
        }

    }


}


