#ifndef BOARDTILETYPECONVERSION_H
#define BOARDTILETYPECONVERSION_H
#include <iostream>
#include "boardtiletype.h"
#include <QString>

class BoardTileTypeConversion
{
public:
    static QString BoardTileTypeToString(BoardTileType type);
    static BoardTileType BoardTileTypeToEnum(std::string type);
    BoardTileTypeConversion();

private:


};

#endif // BOARDTILETYPECONVERSION_H
