import QtQuick 2.0


Rectangle {
    property alias tileText: label.text
    property alias tileIcon: tileImage.source
    id: boardTile
    width: (playingBoard.width / boardCols ) - boardSpacing
    height: (playingBoard.height / boardRows ) - boardSpacing
    color: "transparent"
    border {
        width: 2
        color: "transparent"
    }
    state: "empty_tile"
    property int position

    states: [
        State {
            name: "empty_tile"
            PropertyChanges {
                target: tileImage
                source: chooseTileIcon("")               
            }
        },

        State {
            name: "house"
            PropertyChanges {
                target: tileImage
                source: chooseTileIcon("HOUSE")
            }
        },

        State {
            name: "church"
            PropertyChanges {
                target: tileImage
                source: chooseTileIcon("CHURCH")
            }
        },

        State {
            name: "blacksmith"
            PropertyChanges {
                target: tileImage
                source: chooseTileIcon("BLACKSMITH")
            }
        },

        State {
            name: "townhall"
            PropertyChanges {
                target: tileImage
                source: chooseTileIcon("TOWNHALL")
            }
        }

    ]

    Text {
        id: label
        text: ""
        anchors.centerIn: parent
        font {
            pixelSize: 26
            bold: true
        }
        color: "white"
    }

    Image {
        id: tileImage
        width: parent.width - 20
        height: width
        anchors.centerIn: parent
    }

    MouseArea {
        anchors.fill: parent
        hoverEnabled: true

    Timer{
        repeat: true
        running: true
        interval: 16
        onTriggered: boardTile.state = game.getTileState(position)
    }


        onClicked: {

            //zmeni stav boardu
            if ((boardTile.state === "empty_tile") && (game.getCurrentToolQString() != "empty_hand")){
                if(game.enoughCredits()){
                    boardTile.state = (game.getCurrentToolQString().substring(6));
                    game.setTileState(position, boardTile.state)
                    game.addPopulation(position)
                    game.deductGoldPieces()
                }
           }
           //niceni
           if ((boardTile.state != "empty_tile") && (game.getCurrentToolQString() === "destroy_building")){
               boardTile.state = "empty_tile"
               game.printPos(position)
               game.decreasePopulation(position)
               game.setTileState(position, "empty_tile")

           }
        }

        onEntered: {
            boardTile.border.color = "white"
        }

        onExited: {
            boardTile.border.color = "transparent"
        }
    }

    function chooseTileIcon(_tileType) {
        // celkem shit-tier code, ale funguje
        const folder = "images/"

        if (_tileType === "HOUSE") {
            return folder + "house_new.png"
        } else {
            if (_tileType === "CHURCH") {
                return folder + "church.png"
            } else {
                if (_tileType === "TOWNHALL") {
                    return folder + "townhall.png"
                } else {
                    if (_tileType === "BLACKSMITH") {
                        return folder + "blacksmith.png"
                    } else {
                        return folder
                    }
                }
            }
        }
    }
    //prevod z nastroje na nazev budovy
    function chooseBuilding(_tool){
        if (_tool === "build_house")
            return "house"
        if (_tool === "build_church")
            return "church"
        if(_tool === "build_blacksmith")
            return "blacksmith"
        if(_tool === "build_townhall")
            return "townhall"
    }
}
